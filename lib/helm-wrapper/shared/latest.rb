###############################################################################

require 'json'
require 'net/http'
require 'singleton'
require 'uri'

###############################################################################

module HelmWrapper

###############################################################################

  module Shared

###############################################################################

    class Latest

###############################################################################

      include Singleton

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @version

###############################################################################

      def version
        @version ||= refresh

        return @version
      end

###############################################################################

      private

###############################################################################

      def refresh
        logger.info("Finding latest available Helm release...")

        response = Net::HTTP.get_response(URI("https://api.github.com/repos/helm/helm/releases/latest"))

        logger.fatal("GitHub API did not return status 200 for latest version check!") if response.code != "200"
        logger.fatal("Response body from GitHub API is not permitted!")                if not response.class.body_permitted?
        logger.fatal("Response body from GitHub API is empty!")                        if response.body.nil?

        begin
          body = JSON.parse(response.body)
        rescue JSON::ParserError
          logger.fatal("Response body from GitHub API is not valid JSON!")
        end


        logger.fatal("GitHub API JSON response did not include latest available Helm tag name!") if not body.key?("tag_name")
        logger.fatal("GitHub API indicated the latest available tag name for Helm is blank!")    if body["tag_name"].empty?

        version = body["tag_name"]

        logger.success("Latest available Helm release found: #{version}")

        return version
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
