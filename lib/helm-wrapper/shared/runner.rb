###############################################################################

require 'open3'

###############################################################################

module HelmWrapper
  #############################################################################

  module Shared
    ###########################################################################

    class Runner
      #########################################################################

      include HelmWrapper::Shared::Logging

      #########################################################################

      @@repo_actions = %w[registry repo]

      #########################################################################

      @auths_attempted = false

      #########################################################################

      attr_reader :auths, :binary, :chart, :config, :repos

      #########################################################################

      def initialize(binary:, chart:, config: nil)
        @binary = binary
        @chart  = chart
        @config = config

        @auths = false
        @repos = false
      end

      #########################################################################

      def init_auths
        logger.fatal('Cannot initialise authenticators without a valid configuration!') if @config.nil?

        @auths_attempted = true

        config.auths.map(&:auth)

        @auths = true
      end

      #########################################################################

      def init_repos(force: false)
        @chart.oci.each_with_index do |oci_repo, oci_index|
          logger.info("Logging into OCI repository: #{oci_repo['name']}")

          username = from_environment(variable: oci_repo['username'])
          password = from_environment(variable: oci_repo['password'])

          @chart.oci_active(active: true, index: oci_index)

          parameters = []
          parameters.append('login')
          parameters.append("\"#{oci_repo['url']}\"")
          parameters.append("--username=\"#{username}\"")
          parameters.append('--password-stdin')
          logger.fatal("Failed to login to Helm OCI repository: #{oci_repo['name']}, url: #{oci_repo['url']}") unless run(
            action: 'registry', parameters: parameters, stdin: password
          )
        end

        if @chart.artefact.length > 0

          @chart.artefact.each_with_index do |artefact_repo, artefact_index|
            logger.info("Adding artefact repository: #{artefact_repo['name']}")

            username = artefact_repo['username'].nil? ? nil : from_environment(variable: artefact_repo['username'])
            password = artefact_repo['password'].nil? ? nil : from_environment(variable: artefact_repo['password'])

            @chart.artefact_active(active: true, index: artefact_index)

            parameters = []
            parameters.append('add')
            parameters.append("\"#{artefact_repo['name']}\"")
            parameters.append("\"#{artefact_repo['url']}\"")
            parameters.append("--username=\"#{username}\"") unless username.nil?
            parameters.append("--password=\"#{password}\"") unless password.nil?
            parameters.append('--force-update') if force
            logger.fatal("Failed to add Helm repository: #{artefact_repo['name']}, url: #{artefact_repo['url']}") unless run(
              action: 'repo', parameters: parameters
            )
          end

          logger.info('Updating artefact repositories...')
          logger.fatal('Failed to update Helm artefact repositories!') unless run(action: 'repo',
                                                                                  parameters: Array.new(['update']))
        end

        @repos = true
      end

      #########################################################################

      def clean(repos: true)
        clean_repos if repos
        clean_auths if @auths_attempted
      end

      #########################################################################

      def delete
        logger.fatal('Cannot Helm delete without a valid configuration!') if @config.nil?
        logger.fatal('Cannot Helm delete before initialising authenticators!') unless auths

        parameters = []
        parameters.append("--namespace=\"#{@config.namespace}\"")
        parameters.append("\"#{@config.release}\"")

        logger.fatal('Helm delete failed!') unless run(action: 'delete', parameters: parameters)
      end

      #########################################################################

      def template
        logger.fatal('Cannot Helm template without a valid configuration!') if @config.nil?
        logger.fatal('Cannot Helm template before initialising repositories!') unless repos

        parameters = []
        parameters.append("--namespace=\"#{@config.namespace}\"")
        parameters.append("\"#{@config.release}\"")
        parameters.append("\"#{@chart.name}\"")
        parameters.append("--version=\"#{@chart.version}\"") unless @chart.version.strip.empty?
        parameters.concat(variable_files)
        parameters.concat(variable_strings)

        logger.fatal('Helm template failed!') unless run(action: 'template', parameters: parameters)
      end

      #########################################################################

      def upgrade(install: true)
        logger.fatal('Cannot Helm upgrade without a valid configuration!') if @config.nil?
        logger.fatal('Cannot Helm upgrade before initialising authenticators!') unless auths
        logger.fatal('Cannot Helm upgrade before initialising repositories!') unless repos

        parameters = []
        parameters.append("--namespace=\"#{@config.namespace}\"")
        parameters.append('--install') if install
        parameters.append("\"#{@config.release}\"")
        parameters.append("\"#{@chart.name}\"")
        parameters.append('--atomic') if @config.atomic
        parameters.append('--wait') if @config.wait
        parameters.append("--timeout=\"#{@config.timeout}\"") if @config.atomic or @config.wait
        parameters.append("--version=\"#{@chart.version}\"") unless @chart.version.strip.empty?
        parameters.concat(variable_files)
        parameters.concat(variable_strings)

        logger.fatal('Helm upgrade failed!') unless run(action: 'upgrade', parameters: parameters)
      end

      #########################################################################

      def lint
        parameters = []
        parameters.append("\"#{@chart.path}\"")

        logger.fatal('Helm validate failed!') unless run(action: 'lint', parameters: parameters)
      end

      #########################################################################

      def package(destination: '.')
        parameters = []
        parameters.append("--destination=\"#{destination}\"")
        parameters.append("\"#{@chart.path}\"")

        logger.fatal('Helm package failed!') unless run(action: 'package', parameters: parameters)
      end

      #########################################################################

      def push(destination:, source: '.')
        logger.fatal('Cannot Helm push before initialising repositories!') unless repos

        package_name = "#{@chart.name}-#{@chart.version}.tgz"
        package_path = File.join(source, package_name)

        logger.fatal("Must package before pushing! Package: #{package_path} not found!") unless File.file?(package_path)

        parameters = []
        parameters.append("\"#{package_path}\"")
        parameters.append("\"#{destination}\"")

        logger.fatal('Helm push failed!') unless run(action: 'push', parameters: parameters)
      end

      #########################################################################

      private

      #########################################################################

      def clean_auths
        @auths = false
        config.auths.map(&:clear)
        @auths_attempted = false
      end

      #########################################################################

      def clean_repos
        @repos = false

        @chart.oci.each do |oci_repo|
          next unless oci_repo['active']

          logger.info("Logging out of OCI repository: #{oci_repo['name']}")

          parameters = []
          parameters.append('logout')
          parameters.append("\"#{oci_repo['url']}\"")
          logger.error("Failed to logout of Helm OCI repository: #{oci_repo['name']}, url: #{oci_repo['url']}") unless run(
            action: 'registry', parameters: parameters
          )
        end

        @chart.artefact.each do |artefact_repo|
          if artefact_repo['active']
            if artefact_repo['password'].nil?
              logger.info("Not removing artefact repository: #{artefact_repo['name']}, no credentials stored.")
            else
              logger.info("Removing artefact repository: #{artefact_repo['name']}")

              parameters = []
              parameters.append('remove')
              parameters.append("\"#{artefact_repo['name']}\"")
              logger.error("Failed to remove Helm repository: #{artefact_repo['name']}, url: #{artefact_repo['url']}") unless run(
                action: 'repo', parameters: parameters
              )
            end
          end
        end
      end

      #########################################################################

      def variable_files
        result = []

        @config.variables.files.each do |file|
          result.append("--values=\"#{file}\"")
        end

        result
      end

      #########################################################################

      def variable_strings
        result = []

        @config.variables.values.each do |key, value|
          result.append("--set=\"#{key}=#{value}\"")
        end

        result
      end

      #########################################################################

      def run(action:, parameters: [], stdin: nil)
        result = false

        parameters.reject! { |item| !item.is_a?(String) or item.strip.empty? }

        cmdline = ["\"#{@binary.path}\"", action].concat(parameters).join(' ')

        if @@repo_actions.include?(action)
          begin
            out, status = Open3.capture2e(cmdline, stdin_data: stdin)
            logger.debug('Helm output:')
            logger.debug(out)
            result = status.success?
          rescue StandardError
            result = false
          end
        else
          logger.info("Starting Helm, action: #{action}")

          fatal('Running Helm with data for stdin can only be used when configuring repos!') unless stdin.nil?
          puts("\n" + ('#' * 80) + "\n\n")

          result = system(cmdline) || false

          puts("\n" + ('#' * 80) + "\n\n")
        end

        result
      end

      #########################################################################

      def from_environment(variable:)
        logger.fatal("Environment variable: #{variable} does not exist") unless ENV.key?(variable)
        logger.fatal("Environment variable: #{variable} is blank!") if ENV[variable].strip.empty?

        ENV[variable].strip
      end

      #########################################################################
    end

    ###########################################################################
  end

  #############################################################################
end

###############################################################################
