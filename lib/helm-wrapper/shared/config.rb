###############################################################################

require 'yaml'

###############################################################################

module HelmWrapper

###############################################################################

  module Shared

###############################################################################

    class Config

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @@config_exts = [ "", ".yaml", ".yml" ]

###############################################################################

      attr_reader :atomic
      attr_reader :auths
      attr_reader :base
      attr_reader :chart
      attr_reader :name
      attr_reader :namespace
      attr_reader :release
      attr_reader :path
      attr_reader :timeout
      attr_reader :variables
      attr_reader :wait

###############################################################################

      def initialize(chart:, options:)
        logger.fatal("Configuration atomic mode must be a boolean!") unless [ true, false ].include?(options["atomic"])

        @atomic = options["atomic"]

        logger.fatal("Configuration base path must be a string!") unless options["base"].kind_of?(String)
        logger.fatal("Configuration base path must not be blank!") if options["base"].strip.empty?

        @base = options["base"]

        logger.fatal("Configuration name must be a string!") unless options["name"].kind_of?(String)
        logger.fatal("Configuration name must not be blank!") if options["name"].strip.empty?

        @name = options["name"]

        logger.fatal("Configuration release name must be a string!") unless options["release"].kind_of?(String)
        logger.fatal("Configuration release name must not be blank!") if options["release"].strip.empty?

        @release = options["release"]

        logger.fatal("Configuration wait timeout must be a string!") unless options["timeout"].kind_of?(String)
        logger.fatal("Configuration wait timeout must not be blank!") if options["timeout"].strip.empty?

        @timeout = options["timeout"]

        logger.fatal("Configuration authenticator for Azure enabled must be a boolean!") unless [ true, false ].include?(options["auth-azure"])

        auth_azure = options["auth-azure"]

        logger.fatal("Configuration authenticator for Azure options must be a hash!") unless options["auth-azure-options"].kind_of?(Hash)

        auth_azure_options = options["auth-azure-options"]

        logger.fatal("Configuration destination namespace must be a string!") unless options["namespace"].kind_of?(String)
        logger.fatal("Configuration destination namespace must not be blank!") if options["namespace"].strip.empty?

        namespace = options["namespace"]

        logger.fatal("Configuration wait mode must be a boolean!") unless [ true, false ].include?(options["wait"])

        wait = options["wait"]

        @chart = chart
        @path  = ::HelmWrapper.find(base: @base, name: @name, exts: @@config_exts, description: "Configuration")
        @wait  = (not @atomic) and wait

        yaml = YAML.load(File.read(@path))
        logger.fatal("Invalid YAML in configuration file: #{@path}") unless yaml.kind_of?(Hash)

        identifers = yaml.key?("identifiers") ? yaml["identifiers"] : Hash.new
        @variables = HelmWrapper::Shared::Variables.new(chart: @chart.name, config: @name, namespace: namespace, release: @release, identifiers: identifers)
        @namespace = @variables.core[:namespace]

        if yaml.key?("globals") then
          logger.fatal("Key 'globals' is not a hash in configuration file: #{@path}") unless yaml["globals"].kind_of?(Hash)
          globals = yaml["globals"]

          @variables.add_variables(variables: globals["variables"]) if globals.key?("variables")
        end

        if yaml.key?("helm") then
          logger.fatal("Key 'helm' is not a hash in configuration file: #{@path}") unless yaml["helm"].kind_of?(Hash)
          helm = yaml["helm"]

          [ "globals", @release ].each do |extra|
            if helm.key?(extra) then
              logger.fatal("Key '#{extra}' under 'helm' is not a hash in configuration file: #{@path}") unless helm[extra].kind_of?(Hash)
              section = helm[extra]

              @variables.add_variables(variables: section["variables"]) if section.key?("variables")
              @variables.add_files(base: @base, files: section["files"]) if section.key?("files")
            end
          end
        end

        @auths = Array.new
        @auths.append(HelmWrapper::Shared::Auths::Azure.new(options: auth_azure_options, variables: @variables)) if auth_azure
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
