###############################################################################

module HelmWrapper

###############################################################################

  module Shared

###############################################################################

    class Binary

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      attr_accessor :name

###############################################################################

      attr_reader :base
      attr_reader :directory
      attr_reader :platform
      attr_reader :version

###############################################################################

      def initialize(options:)
        logger.fatal("Binary base path must be a string!") unless options["base"].kind_of?(String)
        logger.fatal("Binary base path must not be blank!") if options["base"].strip.empty?

        @base = options["base"]

        logger.fatal("Binary version must be a string!") unless options["version"].kind_of?(String)
        logger.fatal("Binary version must not be blank!") if options["version"].strip.empty?

        @version = options["version"]

        @platform = platform_detect

        @directory = File.join(@base, @version, @platform)
        @name = "helm"
      end

###############################################################################

      def check()
        result = false

        if exists and executable then
          result = system("\"#{path}\" version --short") || false
        end

        return result
      end

###############################################################################

      def exists()
        return File.exist?(path)
      end

###############################################################################

      def executable()
        return File.executable?(path)
      end

###############################################################################

      def path()
        return File.join(@directory, @name)
      end

###############################################################################

      private

###############################################################################

      def platform_detect
        return "darwin" if platform_is_mac
        return "linux"  if platform_is_linux
        logger.fatal("Platform is NOT supported: #{RUBY_PLATFORM}")
      end

###############################################################################

      def platform_is_mac
        RUBY_PLATFORM =~ /darwin/
      end

###############################################################################

      def platform_is_linux
        RUBY_PLATFORM =~ /linux/
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
