###############################################################################

module HelmWrapper

###############################################################################

  module Shared

###############################################################################

    class Chart

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @@chart_metadata_file = "Chart.yaml"

###############################################################################

      attr_reader :artefact
      attr_reader :name
      attr_reader :path
      attr_reader :oci
      attr_reader :version

###############################################################################

      def initialize(options:)
        if options["path"].nil? then
          @path = options["path"]

          logger.fatal("Chart name must be a string!") unless options["name"].kind_of?(String)
          logger.fatal("Chart name must not be blank!") if options["name"].strip.empty?
          @name = options["name"]

          logger.fatal("Chart version must be a string!") unless options["version"].kind_of?(String)
          @version = options["version"]
        else
          logger.fatal("Chart path must be a string!") unless options["path"].kind_of?(String)
          logger.fatal("Chart path must not be blank!") if options["path"].strip.empty?
          logger.fatal("Chart path must exist!") unless File.directory?(options["path"])

          @path = options["path"]

          load_metadata
        end

        logger.fatal("Chart repos must be a list of hashes!") unless options["repos"].kind_of?(Array)

        repos = options["repos"]

        @oci      = Array.new
        @artefact = Array.new

        repos.each do |repo|
          logger.fatal("All elements of chart repos must be hashes!") unless repo.kind_of?(Hash)

          logger.fatal("Chart repo: #{hash["name"]} must have a type attribute!") unless repo.key?("type")
          logger.fatal("Chart repo: #{hash["name"]} type must be a string!") unless repo["type"].kind_of?(String)
          logger.fatal("Chart repo: #{hash["name"]} type must either be 'artefact' or 'oci'.") unless [ "artefact", "oci" ].include?(repo["type"])

          is_artefact = repo["type"] == "artefact" ? true : false
          is_oci      = repo["type"] == "oci"      ? true : false

          hash = Hash.new

          logger.fatal("Chart repos must have a name attribute!") unless repo.key?("name")
          logger.fatal("Chart repo name must be a string!") unless repo["name"].kind_of?(String)
          logger.fatal("Chart repo name must not be blank!") if repo["name"].strip.empty?

          hash["name"] = repo["name"].strip

          logger.fatal("Chart repo: #{hash["name"]} must have a url attribute!") unless repo.key?("url")
          logger.fatal("Chart repo: #{hash["name"]} url must be a string!") unless repo["url"].kind_of?(String)
          logger.fatal("Chart repo: #{hash["name"]} url must not be blank!") if repo["url"].strip.empty?

          hash["url"] = repo["url"].strip

          if repo.key?("username") then
            logger.fatal("Chart repo: #{hash["name"]} username must be a string!") unless repo["username"].kind_of?(String)
            logger.fatal("Chart repo: #{hash["name"]} username must not be blank!") if repo["username"].strip.empty?

            hash["username"] = repo["username"].strip

            logger.fatal("Chart repo: #{hash["name"]} must have a password attribute if 'username' is set!") unless repo.key?("password")
            logger.fatal("Chart repo: #{hash["name"]} password must be a string!") unless repo["password"].kind_of?(String)
            logger.fatal("Chart repo: #{hash["name"]} password must not be blank!") if repo["password"].strip.empty?

            hash["password"] = repo["password"].strip
          else
            fatal("Chart repo: #{hash["name"]} is an OCI repository therefore must have a username attribute! Public OCI repositories do not need to be declared.") if is_oci
            hash["username"] = nil
            hash["password"] = nil
          end

          hash["active"] = false

          artefact.append(hash) if is_artefact
          oci.append(hash)      if is_oci
        end
      end

###############################################################################

      def artefact_active(active:, index:)
        artefact[index]["active"] = active
      end

###############################################################################

      def oci_active(active:, index:)
        oci[index]["active"] = active
      end

###############################################################################

      private

###############################################################################

      def load_metadata
        file = File.join(@path, @@chart_metadata_file)

        if File.file?(file)
          logger.info("Loading chart metadata from: #{file}...")
        else
          logger.fatal("Chart metadata file: #{file} not found!")
        end

        yaml = YAML.load(File.read(file))
        logger.fatal("Invalid YAML in chart metadata file: #{file}") unless yaml.kind_of?(Hash)

        logger.fatal("Chart name is missing from chart metadata file!") unless yaml.key?("name")
        logger.fatal("Chart name is not a string in chart metadata file!") unless yaml["name"].kind_of?(String)
        logger.fatal("Chart name is blank in chart metadata file!") if yaml["name"].strip.empty?

        @name = yaml["name"]

        logger.fatal("Chart version is missing from chart metadata file!") unless yaml.key?("version")
        logger.fatal("Chart version is not a string in chart metadata file!") unless yaml["version"].kind_of?(String)
        logger.fatal("Chart version is blank in chart metadata file!") if yaml["version"].strip.empty?

        @version = yaml["version"]

        logger.info("Metadata loaded: #{@name} version: #{@version}")
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
