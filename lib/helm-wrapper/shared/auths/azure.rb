###############################################################################

require 'tempfile'

###############################################################################

module HelmWrapper

###############################################################################

  module Shared

###############################################################################

    module Auths

###############################################################################

      class Azure < Common

###############################################################################

        include HelmWrapper::Shared::Logging

###############################################################################

        @@az   = "az"
        @@type = "azure"

###############################################################################

        @keyvault
        @secret_ca
        @secret_endpoint
        @secret_token

###############################################################################

        @ca_tempfile = nil

###############################################################################

        def initialize(options:, variables:)
          construct(options: options, variables: variables)
        end

###############################################################################

        def auth()
          ca       = secret(vault: @keyvault, name: @secret_ca)
          endpoint = secret(vault: @keyvault, name: @secret_endpoint)
          token    = secret(vault: @keyvault, name: @secret_token)

          @ca_tempfile = Tempfile.new('helm-wrapper-auths-azure-ca')

          begin
            @ca_tempfile.write(ca)
          ensure
            @ca_tempfile.close
          end

          logger.success("Azure authenticator successfully written Kubernetes CA to file!")

          ENV["HELM_KUBECAFILE"]    = @ca_tempfile.path
          ENV["HELM_KUBEAPISERVER"] = endpoint
          ENV["HELM_KUBETOKEN"]     = token

          logger.success("Azure authenticator environment variables set!")
        end

###############################################################################

        def clear()
          unless @ca_tempfile.nil?
            @ca_tempfile.unlink
            @ca_tempfile = nil

            logger.info("Azure authenticator Kubernetes CA file cleared!")
          end

          ENV.delete("HELM_KUBECAFILE")
          ENV.delete("HELM_KUBEAPISERVER")
          ENV.delete("HELM_KUBETOKEN")

          logger.info("Azure authenticator environment variables cleared!")
        end

###############################################################################

        private

###############################################################################

        def cli()
          output  = logger.colour ? "yamlc" : "yaml"
          cmdline = "\"#{@@az}\" version --output \"#{output}\""
          return(system(cmdline) || false)
        end

###############################################################################

        def secret(vault:, name:)
          logger.info("Getting secret: #{name}, from key vault: #{vault}...")

          cmdline = "\"#{@@az}\" keyvault secret show --vault-name \"#{vault}\" --name \"#{name}\" --query \"value\" --output \"tsv\""
          stdout  = `#{cmdline}`
          code    = $?.exitstatus

          logger.fatal("Failed to get secret: #{name} from key vault: #{vault}!") if (code != 0 or stdout.strip.empty?)

          return(stdout.strip)
        end

###############################################################################

        def specific()
          logger.fatal("Azure CLI must be installed and accessible to use the Azure authenticator.") unless cli

          logger.fatal("Azure authenticator mandatory option 'keyvault' has not been set!") unless @options.key?("keyvault")
          logger.fatal("Azure authenticator keyvault must be a string!") unless @options["keyvault"].kind_of?(String)
          logger.fatal("Azure authenticator keyvault must not be blank!") if @options["keyvault"].strip.empty?

          keyvault = @options["keyvault"]
          if @options.key?("ca-secret") then
            logger.fatal("Azure authenticator keyvault secret for Kubernetes CA must be a string!") unless @options["ca-secret"].kind_of?(String)
            logger.fatal("Azure authenticator keyvault secret for Kubernetes CA must not be blank!") if @options["ca-secret"].strip.empty?

            ca = @options["ca-secret"]
          else
            ca = "kubernetes-ca"
          end

          if @options.key?("endpoint-secret") then
            logger.fatal("Azure authenticator keyvault secret for endpoint must be a string!") unless @options["endpoint-secret"].kind_of?(String)
            logger.fatal("Azure authenticator keyvault secret for endpoint must not be blank!") if @options["endpoint-secret"].strip.empty?

            endpoint = @options["endpoint-secret"]
          else
            endpoint = "kubernetes-endpoint"
          end

          if @options.key?("token-secret") then
            logger.fatal("Azure authenticator keyvault secret for token must be a string!") unless @options["token-secret"].kind_of?(String)
            logger.fatal("Azure authenticator keyvault secret for token must not be blank!") if @options["token-secret"].strip.empty?

            token = @options["token-secret"]
          else
            token = "kubernetes-token"
          end

          begin
            @keyvault        = keyvault % @variables.identifiers
            @secret_ca       = ca       % @variables.identifiers
            @secret_endpoint = endpoint % @variables.identifiers
            @secret_token    = token    % @variables.identifiers
          rescue
            logger.fatal("Azure authenticator options contain identifiers that are not included in the configuration file!")
          end
        end

###############################################################################

      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
