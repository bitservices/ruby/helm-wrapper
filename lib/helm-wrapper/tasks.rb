###############################################################################

require 'rake/tasklib'

###############################################################################

require_relative 'tasks/apply'
require_relative 'tasks/binary'
require_relative 'tasks/destroy'
require_relative 'tasks/push'
require_relative 'tasks/template'
require_relative 'tasks/validate'

###############################################################################
