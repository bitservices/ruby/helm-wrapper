###############################################################################

module HelmWrapper

###############################################################################

  module Tasks

###############################################################################

    class Apply < ::Rake::TaskLib

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @binary
      @chart
      @options

###############################################################################

      def initialize(binary:, chart:, options:)
        @binary  = binary
        @chart   = chart
        @options = options

        yield self if block_given?

        apply_task
      end

###############################################################################

      def apply_task
        desc "Applies chart with Helm for a given configuration."
        task :apply, [:config, :clean] => :binary do |t, args|
          options = @options.merge({"name" => args[:config]})
          clean   = args[:clean].kind_of?(String) ? args[:clean].downcase == "true" : true

          logger.info("Processing configuration for Helm apply...")

          config = HelmWrapper::Shared::Config.new(chart: @chart, options: options)
          runner = HelmWrapper::Shared::Runner.new(binary: @binary, chart: @chart, config: config)

          logger.info("Running Helm upgrade for release: #{config.release}, namespace: #{config.namespace}...")

          begin
            runner.init_repos
            runner.init_auths
            runner.upgrade
          ensure
            runner.clean(repos: clean)
          end
        end
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
