###############################################################################

module HelmWrapper

###############################################################################

  module Tasks

###############################################################################

    class Push < ::Rake::TaskLib

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @binary
      @chart
      @destination

###############################################################################

      def initialize(binary:, chart:, destination:)
        @binary      = binary
        @chart       = chart
        @destination = destination

        yield self if block_given?

        push_task
      end

###############################################################################

      def push_task
        desc "Pushes a Helm chart to an OCI Helm repository."
        task :push, [:clean] => :binary do |t, args|
          clean = args[:clean].kind_of?(String) ? args[:clean].downcase == "true" : true

          runner = HelmWrapper::Shared::Runner.new(binary: @binary, chart: @chart)

          logger.info("Running Helm push for path: #{@chart.path}...")

          begin
            runner.init_repos
            runner.package
            runner.push(destination: @destination)
          ensure
            runner.clean(repos: clean)
          end
        end
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
