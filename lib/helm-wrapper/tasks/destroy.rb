###############################################################################

module HelmWrapper

###############################################################################

  module Tasks

###############################################################################

    class Destroy < ::Rake::TaskLib

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @binary
      @chart
      @options

###############################################################################

      def initialize(binary:, chart:, options:)
        @binary  = binary
        @chart   = chart
        @options = options

        yield self if block_given?

        destroy_task
      end

###############################################################################

      def destroy_task
        desc "Removes a chart release with Helm for a given configuration."
        task :destroy, [:config] => :binary do |t, args|
          options = @options.merge({"name" => args[:config]})

          logger.info("Processing configuration for Helm destroy...")

          config = HelmWrapper::Shared::Config.new(chart: @chart, options: options)
          runner = HelmWrapper::Shared::Runner.new(binary: @binary, chart: @chart, config: config)

          logger.info("Running Helm delete for release: #{config.release}, namespace: #{config.namespace}...")

          begin
            runner.init_auths
            runner.delete
          ensure
            runner.clean(repos: false)
          end
        end
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
