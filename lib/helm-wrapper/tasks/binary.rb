###############################################################################

require 'digest'
require 'fileutils'
require 'net/http'
require 'rubygems/package'
require 'uri'
require 'zlib'

###############################################################################

module HelmWrapper

###############################################################################

  module Tasks

###############################################################################

    class Binary < ::Rake::TaskLib

###############################################################################

      include HelmWrapper::Shared::Logging

###############################################################################

      @binary

###############################################################################

      def initialize(binary:)
        @binary = binary

        yield self if block_given?

        binary_task
      end

###############################################################################

      def binary_task
        desc "Downloads and extracts the expected version of the Helm binary if it is not already present."
        task :binary do |t, args|
          logger.info("Checking Helm binary for platform: #{@binary.platform}, version: #{@binary.version}")

          if not @binary.exists then
            logger.info("Helm binary not found. Preparing binary...")

            logger.fatal("Failed to create binary directory: #{directory}") unless ::HelmWrapper.create_directory(directory: @binary.directory, purpose: "binaries")

            archive_binary = "#{@binary.platform}-amd64/helm"
            archive_file   = "helm-#{@binary.version}-#{@binary.platform}-amd64.tar.gz"
            archive_path   = File.join(@binary.directory, archive_file)
            archive_uri    = "https://get.helm.sh/#{archive_file}"

            sums_file = "#{archive_file}.sha256sum"
            sums_path = File.join(@binary.directory, sums_file)
            sums_uri  = "https://get.helm.sh/#{sums_file}"

            begin
              download(path: archive_path, uri: archive_uri) if not File.file?(archive_path)
              download(path: sums_path, uri: sums_uri) if not File.file?(sums_path)
              verify(file: archive_file, path: archive_path, sums: sums_path)
              extract(archive: archive_path, binary: archive_binary, destination: @binary.path)
            ensure
              clean(archive: archive_path, sums: sums_path)
            end
          end

          if not @binary.executable then
            logger.info("Helm binary not executable. Setting permissions...")
            executable(path: @binary.path)
          end

          logger.fatal("Problem with checking the Helm binary!") unless @binary.check
        end
      end

###############################################################################

      private

###############################################################################

      def download(path:, uri:)
        logger.info("Downloading: #{uri}")

        response = Net::HTTP.get_response(URI(uri))

        logger.fatal("Download request did not return HTTP status 200!") if response.code != "200"
        logger.fatal("Download response body is not permitted!")         unless response.class.body_permitted?
        logger.fatal("Download response body is empty!")                 if response.body.nil?

        open(path, "wb") { |file|
          file.write(response.body)
        }

        logger.fatal("Download failed!") unless File.file?(path)
      end

###############################################################################

      def verify(file:, path:, sums:)
        logger.info("Checking SHA256 for: #{file}")

        result = false

        sha256 = Digest::SHA256.hexdigest File.read(path)

        File.readlines(sums).each do |line|
          begin
            fields     = line.match /^(?<sum>\S+)\s+(?<file>\S+)$/
            sum_file   = fields["file"]
            sum_sha256 = fields["sum"]
          rescue
            logger.warn("Unexpected data in sums file: #{sums}")
            next
          end

          if sum_file == file then
            logger.info("Expected SHA256 sum: #{sum_sha256}")
            logger.info("Actual SHA256 sum: #{sha256}")
            result = (sum_sha256 == sha256)
            break
          end
        end

        logger.fatal("Error whilst verifying the SHA256 sum of the downloaded Helm archive!") unless result
      end

###############################################################################

      def extract(archive:, binary:, destination:)
        logger.info("Extracting: #{archive}")

        longlink = "././@LongLink"

        Gem::Package::TarReader.new(Zlib::GzipReader.open(archive)) do |tar|
          cursor = nil
          tar.each do |file|
            if file.full_name == longlink then
              cursor = file.read.strip
            else
              cursor ||= file.full_name.strip
              if file.file? and (cursor == binary) then
                File.open(destination, 'wb') do |output|
                  output.write(file.read)
                end
              end
              cursor = nil
            end
          end
        end

        logger.fatal("Extraction of Helm binary: #{binary}, from archive: #{archive} has failed!") unless File.file?(destination)
      end

###############################################################################

      def executable(path:)
        logger.info("Making executable: #{path}")
        FileUtils.chmod("+x", path)
        logger.fatal("Setting executable bit on file: #{path} has failed!") unless File.executable?(path)
      end

###############################################################################

      def clean(archive:, sums:)
        [archive, sums].each do |file|
          if File.file?(file)
            logger.info("Removing file: #{file}")

            begin
              File.delete(file)
            rescue
              logger.error("Failed to delete: #{file}, please remove manually.")
            end
          end
        end
      end

###############################################################################

    end

###############################################################################

  end

###############################################################################

end

###############################################################################
