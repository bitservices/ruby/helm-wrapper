###############################################################################

require_relative 'helm-wrapper/shared'
require_relative 'helm-wrapper/tasks'
require_relative 'helm-wrapper/version'

###############################################################################

require_relative 'helm-wrapper/common'

###############################################################################

module HelmWrapper

###############################################################################

  @logger = HelmWrapper::Shared::Logging.logger_for("HelmWrapper")

###############################################################################

  @logger.info("Helm Wrapper for Ruby - version: #{HelmWrapper::VERSION}")

###############################################################################

  def self.deployment_tasks(chart:, namespace:, release:, options: Hash.new)
    @logger.info("Building deployment tasks for release: #{release}...")

    @logger.fatal("Chart must be specified as a string!") unless chart.kind_of?(String)
    @logger.fatal("Options must be specified as a hash!") unless options.kind_of?(Hash)

    binary_options = Hash.new
    binary_options["base"]    = options.key?("binary-base")    ? options["binary-base"]    : File.join(Dir.pwd, "vendor", "helm")
    binary_options["version"] = options.key?("binary-version") ? options["binary-version"] : Shared::Latest.instance.version

    chart_options = Hash.new
    chart_options["name"]    = chart
    chart_options["path"]    = nil
    chart_options["repos"]   = options.key?("chart-repos")   ? options["chart-repos"]   : Array.new
    chart_options["version"] = options.key?("chart-version") ? options["chart-version"] : String.new

    config_options = Hash.new
    config_options["atomic"]             = options.key?("config-atomic")             ? options["config-atomic"]             : false
    config_options["auth-azure"]         = options.key?("config-auth-azure")         ? options["config-auth-azure"]         : false
    config_options["auth-azure-options"] = options.key?("config-auth-azure-options") ? options["config-auth-azure-options"] : Hash.new
    config_options["base"]               = options.key?("config-base")               ? options["config-base"]               : File.join(Dir.pwd, "config")
    config_options["namespace"]          = namespace
    config_options["release"]            = release
    config_options["timeout"]            = options.key?("config-timeout")            ? options["config-timeout"]            : "5m0s"
    config_options["wait"]               = options.key?("config-wait")               ? options["config-wait"]               : true

    binary = HelmWrapper::Shared::Binary.new(options: binary_options)
    chart  = HelmWrapper::Shared::Chart.new(options: chart_options)

    tasks = Array.new
    tasks << HelmWrapper::Tasks::Apply.new(binary: binary, chart: chart, options: config_options)
    tasks << HelmWrapper::Tasks::Binary.new(binary: binary)
    tasks << HelmWrapper::Tasks::Destroy.new(binary: binary, chart: chart, options: config_options)
    tasks << HelmWrapper::Tasks::Template.new(binary: binary, chart: chart, options: config_options)
    return tasks
  end

###############################################################################

  def self.development_tasks(path:, destination: String.new, options: Hash.new)
    @logger.info("Building development tasks for path: #{path}...")

    @logger.fatal("Path must be specified as a string!") unless path.kind_of?(String)
    @logger.fatal("Destination must be specified as a string!") unless destination.kind_of?(String)
    @logger.fatal("Options must be specified as a hash!") unless options.kind_of?(Hash)

    binary_options = Hash.new
    binary_options["base"]    = options.key?("binary-base")    ? options["binary-base"]    : File.join(Dir.pwd, "vendor", "helm")
    binary_options["version"] = options.key?("binary-version") ? options["binary-version"] : Shared::Latest.instance.version

    chart_options = Hash.new
    chart_options["name"]  = nil
    chart_options["path"]  = path
    chart_options["repos"] = options.key?("chart-repos")   ? options["chart-repos"]   : Array.new

    binary = HelmWrapper::Shared::Binary.new(options: binary_options)
    chart  = HelmWrapper::Shared::Chart.new(options: chart_options)

    tasks = Array.new
    tasks << HelmWrapper::Tasks::Binary.new(binary: binary)
    tasks << HelmWrapper::Tasks::Push.new(binary: binary, chart: chart, destination: destination) unless destination.strip.empty?
    tasks << HelmWrapper::Tasks::Validate.new(binary: binary, chart: chart)
    return tasks
  end

###############################################################################

end

###############################################################################
