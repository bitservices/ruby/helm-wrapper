###############################################################################

require_relative '../lib/helm-wrapper/shared'
require_relative '../lib/helm-wrapper/tasks'
require_relative '../lib/helm-wrapper/version'

###############################################################################

require_relative '../lib/helm-wrapper/common'

###############################################################################

require_relative 'spec_logger'

###############################################################################

RSpec.configure do |config|

###############################################################################

  config.before {
    allow_any_instance_of(Logger).to receive(:fatal).and_return(nil)
    allow_any_instance_of(Logger).to receive(:error).and_return(nil)
    allow_any_instance_of(Logger).to receive(:warn).and_return(nil)
    allow_any_instance_of(Logger).to receive(:info).and_return(nil)
  }

###############################################################################

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

###############################################################################

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

###############################################################################

  config.shared_context_metadata_behavior = :apply_to_host_groups

###############################################################################

end

###############################################################################
