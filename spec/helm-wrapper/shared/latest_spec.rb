###############################################################################

shared_examples 'latest version invalid response from github' do |name|
  describe "invalid response #{name}" do
    before(:each) do
      response = Net::HTTPResponse.new(http_version, code.to_s, message)
      allow_any_instance_of(Net::HTTP).to receive(:request) { response }
      allow(response).to receive(:body) { body }
      stub_const('Net::HTTPResponse::HAS_BODY', true)
    end

    it 'must exit' do
      expect { HelmWrapper::Shared::Latest.instance.version }.to raise_error(SystemExit)
    end

    it 'must have error code 1' do
      begin
        HelmWrapper::Shared::Latest.instance.version
      rescue SystemExit => e
        expect(e.status).to eq(1)
      end
    end
  end
end

###############################################################################

describe HelmWrapper::Shared::Latest do
  before(:each) do
    @http_version = '1.1'
  end

  it_behaves_like 'latest version invalid response from github', '404 not found' do
    let(:http_version) { @http_version }
    let(:code)         { 404 }
    let(:message)      { 'Not Found' }
    let(:body)         { '{"error":"Not Found"}' }
  end

  it_behaves_like 'latest version invalid response from github', 'invalid JSON' do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { 'OK' }
    let(:body)         { 'Not JSON' }
  end

  it_behaves_like 'latest version invalid response from github', 'no version in JSON' do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { 'OK' }
    let(:body)         { '{"status":"ok"}' }
  end

  it_behaves_like 'latest version invalid response from github', 'blank version in JSON' do
    let(:http_version) { @http_version }
    let(:code)         { 200 }
    let(:message)      { 'OK' }
    let(:body)         { '{"tag_name":""}' }
  end

  describe 'latest version valid response from github' do
    it 'must return the version provided by github' do
      response = Net::HTTPResponse.new(@http_version, '200', 'OK')
      allow_any_instance_of(Net::HTTP).to receive(:request) { response }
      allow(response).to receive(:body) { '{"tag_name":"1.0.0"}' }
      stub_const('Net::HTTPResponse::HAS_BODY', true)
      expect(HelmWrapper::Shared::Latest.instance.version).to eql('1.0.0')
    end
  end
end

###############################################################################
